Với gần 31 triệu người dùng internet, chiếm 34% dân số, Việt Nam là thị trường màu mỡ để phát triển các dịch vụ thương mại điện tử. Đây cũng là khởi đầu thuận lợi để các dịch vụ mua sắm qua mạng nở rộ.

Ở góc độ nhìn nhận của người tiêu dùng, theo kết quả "Nghiên cứu Giám sát Người tiêu dùng với Thương mại điện tử năm 2012" do Visa thực hiện vừa công bố cũng cho thấy,  có 98% người tham gia nghiên cứu tìm kiếm sản phẩm và dịch vụ trên mạng trong vòng 12 tháng qua. Trong đó, 71% đã mua hàng trực tuyến và 90% đối tượng khảo sát cho biết họ sẽ tiếp tục sử dụng cách mua bán này trong tương lai.
 
Điều này cho thấy người tiêu dùng Việt ngày càng ưa chuộng mua sắm online và tin tưởng hơn vào các biện pháp bảo mật trực tuyến của các website. Đây cũng là điểm then chốt để các doanh nghiệp thương mại điện tử tạo dựng uy tín và nâng cao năng lực cạnh tranh so với các đối thủ.

Cùng với đó, sự phổ biến của điện thoại smartphone và mạng 3G cũng là nền tảng thúc đẩy sự phát triển của thương mại điện tử.39% đối tượng khảo sát sử dụng các thiết bị điện thoại di động để tìm kiếm sản phẩm và dịch vụ online. Họ sử dụng điện thoại di động cho các mục đích chính như thanh toán hóa đơn, chơi game trực tuyến và các nội dung kỹ thuật số khác…

Tuy nhiên, với việc phát triển rầm rộ của các trang web mua sắm online, bên cạnh những web uy tín thì cũng có những web không được chất lượng lắm. Nhằm giúp người tiêu dùng tránh được sự phân vân cho những lựa chọn của mình nay chúng tôi sẽ đưa ra những tiện ích cho người dùng khi tìm kiếm shop online dựa theo tiêu chí "thân thiện, đơn giản, lựa chọn nhanh chóng".
* Một số tính năng đặc biệt của web dành cho người dùng:
	- Đưa các website được nhiều người tiêu dùng bình chọn nhất.
	- Gợi ý các website gần với địa điểm người dùng nhất.
	- Lưu lại các lịch sử truy cập người dùng qua đó đưa ra các shop cùng chủ đề mà người dùng đó quan tâm.
	- Cho phép người dùng có thể tìm kiếm sản phẩm dễ dàng.
	- Nhiều tính năng khác giúp ngưởi dùng cảm thấy thoải mái nhất khi sử dụng.

Qua những điều trên tôi tin rằng sẽ có nhiều người truy cập vào trang web này. Hiện nay thì trên internet khá ít websites có cùng chủ đề và chức năng này nên tính cạnh tranh sẽ không cao, giúp ta dễ dàng chiếm được lòng tin người dùng. Các nhà đầu tư có thể yên tâm khi đầu tư vào dự án này.	
