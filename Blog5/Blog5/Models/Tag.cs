﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog5.Models
{
    public class Tag
    {

        public int TagID { set; get; }
       // [Required]
       // [StringLength(100, MinimumLength = 3, ErrorMessage = "Từ 3 - 100 kí tự!")]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}