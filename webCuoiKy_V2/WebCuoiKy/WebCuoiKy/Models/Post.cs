﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebCuoiKy.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required]
        [StringLength(10, ErrorMessage = "Số kí trong khoảng 4->10 kí tự!", MinimumLength = 4)]
        public string TieuDe { set; get; }
        public string LinkWeb { set; get; }
        public string DCShop { set; get; }
        public int LuotLike { set; get; }
        public byte[] HinhAnh { set; get; }
        public DateTime DateCreate { set; get; }
        // [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày giờ!!!")]
        public int ChuyenMucID { set; get; }
        public virtual ChuyenMuc chuyenmuc { set; get; }
        public int ThanhPhoID { set; get; }
        public virtual ThanhPho thanhpho { set; get; }
        //
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserID { set; get; }
        //
        public virtual ICollection<Tag> Tags { set; get; }
        //
        public virtual ICollection<Comment> Comments { set; get; }
    }
}