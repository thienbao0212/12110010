﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebCuoiKy.Models
{
    public class ChuyenMuc
    {
        public int id { set; get; }
        public string tenChuyenMuc { set; get; }
        //public int PostID { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}