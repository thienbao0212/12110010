namespace WebCuoiKy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LinkWeb = c.String(),
                        DCShop = c.String(),
                        LuotLike = c.Int(nullable: false),
                        HinhAnh = c.Binary(),
                        DateCreate = c.DateTime(nullable: false),
                        ChuyenMucID = c.Int(nullable: false),
                        ThanhPhoID = c.Int(nullable: false),
                        UserProfileUserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ChuyenMucs", t => t.ChuyenMucID, cascadeDelete: true)
                .ForeignKey("dbo.ThanhPhoes", t => t.ThanhPhoID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserID, cascadeDelete: true)
                .Index(t => t.ChuyenMucID)
                .Index(t => t.ThanhPhoID)
                .Index(t => t.UserProfileUserID);
            
            CreateTable(
                "dbo.ChuyenMucs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        tenChuyenMuc = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ThanhPhoes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        tenThanhPho = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.TagID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        DateCreate = c.DateTime(nullable: false),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.TagPosts",
                c => new
                    {
                        Tag_TagID = c.Int(nullable: false),
                        Post_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagID, t.Post_ID })
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .ForeignKey("dbo.Posts", t => t.Post_ID, cascadeDelete: true)
                .Index(t => t.Tag_TagID)
                .Index(t => t.Post_ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TagPosts", new[] { "Post_ID" });
            DropIndex("dbo.TagPosts", new[] { "Tag_TagID" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "UserProfileUserID" });
            DropIndex("dbo.Posts", new[] { "ThanhPhoID" });
            DropIndex("dbo.Posts", new[] { "ChuyenMucID" });
            DropForeignKey("dbo.TagPosts", "Post_ID", "dbo.Posts");
            DropForeignKey("dbo.TagPosts", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "UserProfileUserID", "dbo.UserProfile");
            DropForeignKey("dbo.Posts", "ThanhPhoID", "dbo.ThanhPhoes");
            DropForeignKey("dbo.Posts", "ChuyenMucID", "dbo.ChuyenMucs");
            DropTable("dbo.TagPosts");
            DropTable("dbo.Comments");
            DropTable("dbo.Tags");
            DropTable("dbo.ThanhPhoes");
            DropTable("dbo.ChuyenMucs");
            DropTable("dbo.Posts");
            DropTable("dbo.UserProfile");
        }
    }
}
