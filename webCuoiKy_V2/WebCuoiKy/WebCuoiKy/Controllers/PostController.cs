﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCuoiKy.Models;

namespace WebCuoiKy.Controllers
{
    public class PostController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Post/

        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.chuyenmuc).Include(p => p.thanhpho).Include(p => p.UserProfile);
            return View(posts.ToList());
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "id", "tenChuyenMuc");
            ViewBag.ThanhPhoID = new SelectList(db.ThanhPhoes, "id", "tenThanhPho");
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                post.DateCreate = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileUserID = userid;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "id", "tenChuyenMuc", post.ChuyenMucID);
            ViewBag.ThanhPhoID = new SelectList(db.ThanhPhoes, "id", "tenThanhPho", post.ThanhPhoID);
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserID);
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "id", "tenChuyenMuc", post.ChuyenMucID);
            ViewBag.ThanhPhoID = new SelectList(db.ThanhPhoes, "id", "tenThanhPho", post.ThanhPhoID);
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ChuyenMucID = new SelectList(db.ChuyenMucs, "id", "tenChuyenMuc", post.ChuyenMucID);
            ViewBag.ThanhPhoID = new SelectList(db.ThanhPhoes, "id", "tenThanhPho", post.ThanhPhoID);
            ViewBag.UserProfileUserID = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}