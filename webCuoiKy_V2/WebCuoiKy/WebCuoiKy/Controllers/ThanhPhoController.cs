﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebCuoiKy.Models;

namespace WebCuoiKy.Controllers
{
    public class ThanhPhoController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /ThanhPho/

        public ActionResult Index()
        {
            return View(db.ThanhPhoes.ToList());
        }

        //
        // GET: /ThanhPho/Details/5

        public ActionResult Details(int id = 0)
        {
            ThanhPho thanhpho = db.ThanhPhoes.Find(id);
            if (thanhpho == null)
            {
                return HttpNotFound();
            }
            return View(thanhpho);
        }

        //
        // GET: /ThanhPho/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ThanhPho/Create

        [HttpPost]
        public ActionResult Create(ThanhPho thanhpho)
        {
            if (ModelState.IsValid)
            {
                db.ThanhPhoes.Add(thanhpho);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(thanhpho);
        }

        //
        // GET: /ThanhPho/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ThanhPho thanhpho = db.ThanhPhoes.Find(id);
            if (thanhpho == null)
            {
                return HttpNotFound();
            }
            return View(thanhpho);
        }

        //
        // POST: /ThanhPho/Edit/5

        [HttpPost]
        public ActionResult Edit(ThanhPho thanhpho)
        {
            if (ModelState.IsValid)
            {
                db.Entry(thanhpho).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(thanhpho);
        }

        //
        // GET: /ThanhPho/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ThanhPho thanhpho = db.ThanhPhoes.Find(id);
            if (thanhpho == null)
            {
                return HttpNotFound();
            }
            return View(thanhpho);
        }

        //
        // POST: /ThanhPho/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ThanhPho thanhpho = db.ThanhPhoes.Find(id);
            db.ThanhPhoes.Remove(thanhpho);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}